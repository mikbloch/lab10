package com.example.accessingdatajpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.sql.Date;
import java.util.Calendar;

@SpringBootApplication
public class AccessingDataJpaApplication {

	private static final Logger log = LoggerFactory.getLogger(AccessingDataJpaApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(AccessingDataJpaApplication.class);
	}

	@Bean
	public CommandLineRunner demo(CustomerRepository repository) {
		return (args) -> {
			// save a few customers
			repository.save(new Customer("Jack", "Bauer", 1.12d, new Date(Calendar.getInstance().getTimeInMillis())));
			repository.save(new Customer("Chloe", "O'Brian", 2.12d, new Date(Calendar.getInstance().getTimeInMillis())));
			repository.save(new Customer("Kim", "Bauer", 12.12d, new Date(Calendar.getInstance().getTimeInMillis())));

			// fetch all customers
			log.info("Customers found with findAll():");
			log.info("-------------------------------");
			for (Customer customer : repository.findAll()) {
				log.info(customer.toString());
			}
			log.info("");

			// fetch an individual customer by ID
			Customer customer = repository.findById(1L);
			log.info("Customer found with findById(1L):");
			log.info("--------------------------------");
			log.info(customer.toString());
			log.info("");

			// fetch customers by last name
			log.info("Customer found with findByLastName('Bauer'):");
			log.info("--------------------------------------------");
			repository.findByLastName("Bauer").forEach(bauer -> {
				log.info(bauer.toString());
			});

			// fetch customers $
			log.info("Customer found with $:");
			log.info("--------------------------------------------");
			repository.findByMoney(1.12d).forEach(bauer -> {
				log.info(bauer.toString());
			});

			// fetch customers Date
			log.info("Customer found with Date:");
			log.info("--------------------------------------------");
			repository.findByDate(new Date(Calendar.getInstance().getTimeInMillis())).forEach(bauer -> {
				log.info(bauer.toString());
			});

			// for (Customer bauer : repository.findByLastName("Bauer")) {
			// 	log.info(bauer.toString());
			// }
			log.info("");
		};
	}

}
