package com.example.accessingdatajpa;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Date;

@Entity
@Data
@NoArgsConstructor
public class Customer {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String firstName;
	private String lastName;
	private Double money;
	private Date date;

	public Customer(String firstName, String lastNamem, Double money, Date date) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.money = money;
		this.date = date;
	}
}
