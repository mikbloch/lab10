package com.example.accessingdatajpa.controller;

import com.example.accessingdatajpa.domain.Customer;
import com.example.accessingdatajpa.service.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class ApiController {
    @Autowired
    private CustomerRepository rep;

    @GetMapping("/api/customer/{id}")
    Optional<Customer> getCustomer(@PathVariable Long id) {
        return rep.findById(id);
    }

    @PostMapping("/api/customer")
    Customer addCustomer(@RequestBody Customer customer) {
        Customer customerToAdd = new Customer(customer.getFirstName(), customer.getLastName(), customer.getMoney(), customer.getDate());
        rep.save(customerToAdd);
        return customerToAdd;
    }

}
