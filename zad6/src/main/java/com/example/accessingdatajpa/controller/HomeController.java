package com.example.accessingdatajpa.controller;

import com.example.accessingdatajpa.domain.Car;
import com.example.accessingdatajpa.domain.Customer;
import com.example.accessingdatajpa.service.CarRepository;
import com.example.accessingdatajpa.service.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.sql.Date;
import java.util.Calendar;

@Controller
public class HomeController {

    @Autowired
    private CustomerRepository kus;
    @Autowired
    private CarRepository kar;

    @GetMapping("/home")
    public String home(Model model){
        model.addAttribute("customer", new Customer());
        model.addAttribute("customers", kus.findAll());
        return "customer-form";
    }

    @PostMapping("/home")
    public String addPerson(Model model, @Valid Customer customer, Errors errors) {
        if(errors.hasErrors()){
            model.addAttribute("customers", kus.findAll());
            return "customer-form";
        }
        customer.setDate(new Date(Calendar.getInstance().getTimeInMillis()));
        kus.save(customer);
        return "redirect:/home";
    }

    @GetMapping("/cars")
    public String homecar(Model model){
        model.addAttribute("cars", kar.findAll());
        return "cars";
    }

    @GetMapping("/cus/{id}")
    public String addcar(Model model, @PathVariable int id){
        model.addAttribute("car", new Car());
        model.addAttribute("id", id);
        return "car-form";
    }

    @PostMapping("/cusad/{id}")
    public String addcar4relz(Model model, Car car, @PathVariable int id){
        car.setCustomer(kus.getById(id));
        kar.save(car);
        return "redirect:/cars";
    }

}
