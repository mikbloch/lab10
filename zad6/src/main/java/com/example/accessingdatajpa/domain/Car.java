package com.example.accessingdatajpa.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
public class Car {
    @Id
    @GeneratedValue
    private int id;
    private String name;
    @ManyToOne(fetch = FetchType.LAZY)
    Customer customer;
}
