package com.example.accessingdatajpa.service;

import java.sql.Date;
import java.util.List;

import com.example.accessingdatajpa.domain.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CustomerRepository extends PagingAndSortingRepository<Customer, Long> {

	List<Customer> findByLastName(String lastName);
	Customer findById(long id);
	Customer getById(long id);


	List<Customer> findByMoney(double money);
	List<Customer> findByDate(Date date);


	Page<Customer> findAll(Pageable page);
	Iterable<Customer> findAll(Sort sort);

}
