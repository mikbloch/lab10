package com.example.accessingdatajpa.controller;

import com.example.accessingdatajpa.domain.Customer;
import com.example.accessingdatajpa.service.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.sql.Date;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@Controller
public class HomeController {

    @Autowired
    private CustomerRepository cr;

    @GetMapping("/home")
    public String home(Model model){
        model.addAttribute("customer", new Customer());
        model.addAttribute("customers", cr.findAll());
        return "customer-form";
    }

    @PostMapping("/home")
    public String addPerson(Model model, @Valid Customer customer, Errors errors) {
        if(errors.hasErrors()){
            model.addAttribute("customers", cr.findAll());
            return "customer-form";
        }
        customer.setDate(new Date(Calendar.getInstance().getTimeInMillis()));
        cr.save(customer);
        return "redirect:/home";
    }
}
