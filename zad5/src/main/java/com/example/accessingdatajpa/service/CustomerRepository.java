package com.example.accessingdatajpa.service;

import java.sql.Date;
import java.util.List;

import com.example.accessingdatajpa.domain.Customer;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

	List<Customer> findByLastName(String lastName);
	Customer findById(long id);
	Customer getById(long id);


	List<Customer> findByMoney(double money);
	List<Customer> findByDate(Date date);

	List<Customer> findAll();

	List<Customer> findAll(Sort sort);

	List<Customer> save(Iterable<? extends Customer> entities);

	void flush();

	Customer saveAndFlush(Customer customer);

	void deleteInBatch(Iterable<Customer> customers);
}
