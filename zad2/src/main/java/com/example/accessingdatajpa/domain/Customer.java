package com.example.accessingdatajpa.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.*;
import java.util.Date;

@Data
@NoArgsConstructor
@Entity
public class Customer {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@NotBlank(message = "nie może być puste")
	@Pattern(regexp = "([A-Z][a-z]+)?", message = "błędne imie")
	private String firstName;

	@NotBlank(message = "nie może być puste")
	@Pattern(regexp = "([A-Z][a-z]+)?", message = "błędne nazwisko")
	private String lastName;

	@NotNull
	@Digits(integer = 100, fraction = 2, message = "zły input")
	@Min(value = 500, message = "kwota musi wynosić przynajmniej 500")
	private double money;
	private Date date;

	public Customer(String firstName, String lastName, Double money, java.sql.Date date) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.money = money;
		this.date = date;
	}
}
